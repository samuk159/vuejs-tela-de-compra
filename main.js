var app = new Vue({
    el: '#app',
    data: {
        brand: 'Nintendo',
        product: 'Controle Remoto sem fio para Nintendo Wii',
        description: 'Novo controlador Remoto com Case Para Nintendo W ii/Para W II U Jogo',
        features: [
            '100% brand new com alta qualidade.',
            'Região livre, ele pode ser usado para qualquer versão do Para Nintendo W ii/Para W II U.',
            'Livre e confortável para mantê-los em qualquer mão.',
            'Compatível com todos os jogos DOS EUA, software, hardware, e tudo mais.',
            'Motor de vibração embutido.',
            'Coloque todos os aparelhos eletrônicos que usam um cabo de carregamento USB.'
        ],
        specifications: [
            'Powered by 2 bateria AA. (NÃO Incluído).',
            'Cor: Branco, Verde, Rosa, Azul, Vermelho',
            'Compatível com: Para Nintendo W ii/Para W II U.'
        ],
        selectedColor: 0,        
        cart: 0,
        colors: [ 
            {
                id: 0,
                value: 'white',
                label: 'Branco',
                cssColor: '#fff',
                quantity: 100
            },
            {
                id: 1,
                value: 'black',
                label: 'Preto',
                cssColor: '#000',
                quantity: 20
            },
            {
                id: 2,
                value: 'red',
                label: 'Vermelho',
                cssColor: '#f00',
                quantity: 1
            },
            {
                id: 3,
                value: 'blue',
                label: 'Azul',
                cssColor: '#00f',
                quantity: 5
            },
            {
                id: 4,
                value: 'pink',
                label: 'Rosa',
                cssColor: '#f6c',
                quantity: 0
            }
        ],
        quantity: 1,
        images: [ 'img1', 'img2' ],
        selectedImage: 'img1'
    },
    methods: {
        addToCart: function() {
            this.cart += Number(this.quantity);
        },
        updateColor: function(index) {
            this.selectedColor = index;
        },
        updateImage: function(image) {
            this.selectedImage = image;
        },
        imagePreview(image) {
            return './assets/' + image + '-' + this.colors[this.selectedColor].value + '.png';
        },
    },
    computed: {
        title() {
            return this.brand + ' - ' + this.product;
        },
        image() {
            return './assets/' + this.selectedImage + '-' + this.colors[this.selectedColor].value + '.png';
        },        
        inStock() {
            return this.colors[this.selectedColor].quantity;
        }
    }
});